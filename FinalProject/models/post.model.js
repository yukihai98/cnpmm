var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var postSchema = new mongoose.Schema({
    category        :{
        type: Schema.Types.ObjectId, ref: 'category'
    },
    id              :{
        type: String
    },
    postTitle       :{
        type: String
    },
    postAuthor      :{
        type: String
    },
    postContent     :{
        type: String, 
    },
    postItemPrice   :{
        type: Number
    },
    postImgURL    :{
        type: Object
    },
    postPlace :{
        type: String
    },
    postCreated      :{
        type: Date
    },
    postUpdated     :{
        type: Date,
        default: Date.now
    },
    postTelephoneNumber:{
        type: String,
    }

});

module.exports = mongoose.model('Post', postSchema);