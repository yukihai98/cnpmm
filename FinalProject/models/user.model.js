const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

var userSchema = new mongoose.Schema({
    fullName : {
        type: String, 
        //required: 'Full name can\'t be empty'
    }, 
    username: {
        type: String,
        //required : 'Username can\'t be empty',
        unique: true,
        minlength: [10, 'Username must be at least 10 characters long']
    },
    email: {
        type: String,
        //required: 'Email can\'t be empty',
    },
    password : {
        type: String,
        //required: 'Password can\'t be empty',
        minlength: [8, 'Password must be at least 8 characters long']
    },
    role    :{
        type: String,
    },
    telephoneNumber: {
        type: String
    },
    saltSecret: String,
    facebook: {
        id: String,
        name: String,
        token: String,
        email: String
    },
    google: {
        id: String,
        name: String,
        token: String,
        email: String
    }
});
//validation for Email
userSchema.path('email').validate((val)=>{
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(val);
}, 'Invalid e-mail');

//Events 
userSchema.pre('save', function(next){
    bcrypt.genSalt(10, (err,salt)=>{
        if(err)
        {
            console.log(err);
            return next.send(err);
        }
        else
        {
            console.log('start hashing');
            bcrypt.hash(this.password, salt, (err,hash)=>{
                this.password = hash;
                this.saltSecret = salt;
                next();
            });
        }
    });
});

//Methods
userSchema.methods.verifyPassword = function (password){
    //console.log(password);
    return bcrypt.compareSync(password, this.password);
}

userSchema.methods.generateJwt = function(){ 
    return jwt.sign({_id: this._id}, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXP
    });
}


mongoose.model('User', userSchema);