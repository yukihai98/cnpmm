var mongoose = require('mongoose');
const moment = require('moment-timezone');
const dateVN = moment.tz(Date.now(), "Asia/Bangkok");

var CategorySchema = new mongoose.Schema({
    id              :{
        type: String, 
    },
    categoryName    :{
        type : String, 
    },
    categoryImgURL  :{
        type : String
    },
    categoryUpdated:{
        type: Date,
        default:  dateVN
    }
});

module.exports = mongoose.model('Category', CategorySchema);