const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller');

const jwtHelper = require('../config/jwtHelpler');

//register
router.post('/register', userController.register);
//login
router.post('/login', userController.login);
//userProfile
router.get('/userProfile', jwtHelper.verifyJwtToken, userController.userProfile);

module.exports = router;