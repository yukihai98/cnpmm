var express = require('express');
var router = express.Router();
var multer = require('multer');
var path = require('path');
var fs = require('fs');


var listImage = [];

var storage = multer.diskStorage({
    destination: (req,file,cb)=>{
        cb(null, './uploaded-image');
    },
    filename: (req,file,cb)=>{
        const fileName = file.fieldname + '-' + Date.now() + path.extname(file.originalname);
        listImage.push(fileName);
        console.log(listImage);
        cb(null, fileName);
    },
});

var upload = multer({storage: storage});
router.post('/upload', upload.array('files') ,(req,res,next)=>{
    const files = req.files;
    console.log(files);
    if(!files)
    {
        const err = new Error('No File');
        err.httpStatusCode = 400;
        return next(err);
    }
    else
    {
        res.send({status: 'send successfully', imageName: listImage});
        listImage= [];
    }
})
//get image 
router.get('/getImage/:imageURL', (req,res)=>{
    var myPath = './uploaded-image/' + req.params.imageURL;
    fs.readFile(myPath, (err,data)=>{
        if(err)
        {
            res.send('Error when reading image');
        }
        else
        {
            res.end(data);
        }
    })
})

module.exports = router;