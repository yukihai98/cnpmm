var express = require('express');
var router = express.Router();
var jwtHelper = require('../config/jwtHelpler');
var postController = require('../controllers/post.controller');

//get All Post
router.get('/', jwtHelper.verifyJwtToken, postController.getAllPost);
//get Post By Id
router.get('/:id', jwtHelper.verifyJwtToken, postController.getPostById);
//create New Post
router.post('/', jwtHelper.verifyJwtToken, postController.createPost);
//update Post
router.put('/:id', jwtHelper.verifyJwtToken, postController.updatePost);
//delete Post
router.delete('/:id', jwtHelper.verifyJwtToken, postController.deletePost);
//

module.exports = router;