var express = require('express');
var router = express.Router();

var categoryController = require('../controllers/category.controller');
var jwtHelper = require('../config/jwtHelpler');
//get All Category
router.get('/', jwtHelper.verifyJwtToken, categoryController.getAllCategory);
//get Category By Id
router.get('/:id', jwtHelper.verifyJwtToken, categoryController.getCategoryById);
//update Category
router.put('/:id', jwtHelper.verifyJwtToken, categoryController.updateCategory);
//delete Category
router.delete('/:id', jwtHelper.verifyJwtToken, categoryController.deleteCategory);
//add category
router.post('/', jwtHelper.verifyJwtToken, categoryController.createCategory);

module.exports = router;