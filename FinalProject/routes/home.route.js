var express = require('express');
var router = express.Router();

var homeController = require('../controllers/home.controller');

router.get('/', homeController.getHomePage);
router.get('/post', homeController.getAllPost);
router.get('/category', homeController.getAllCategory);
router.get('/bycategory/:id', homeController.getAllPostByCategoryId);
router.get('/post/:id', homeController.getPostById);

module.exports = router;