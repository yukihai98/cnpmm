var postModel = require('../models/post.model');
var categoryModel = require('../models/category.model');

module.exports.getHomePage = (req,res,next)=>{
    res.render('index', {title: Express});
};

module.exports.getAllCategory = (req,res,next)=>{
    categoryModel.find((err,categories)=>{
        if(err)
        {
            return next(err);
        }
        else
        {
            res.json(categories);
        }
    });
};

module.exports.getAllPost = (req,res,next)=>{
    postModel.find((err,posts)=>{
        if(err)
        {
            return next(err);
        }
        else
        {
            res.json(posts);
        }
    });
};

module.exports.getAllPostByCategoryId = (req,res,next)=>{
    categoryModel.find({category: req.params.id},(err,posts)=>{
        if(err)
        {
            return next(err);
        }
        else
        {
            res.json(posts);
        }
    });
};

module.exports.getPostById = (req,res,next)=>{
    postModel.findById(req.params.id, (err,post)=>{
        if(err)
        {
            return next(err);
        }
        else
        {
            res.json(post);
        }
    });
};

