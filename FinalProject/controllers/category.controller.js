var categoryModel = require('../models/category.model');


module.exports.getAllCategory = (req,res,next)=>{
    categoryModel.find((err,categories)=>{
        if(err)
        {
            return next(err);
        }
        else
        {
            res.json(categories);
        }
    });
};

module.exports.getCategoryById = (req,res,next)=>{
    categoryModel.findById(req.params.id,(err,category)=>{
        if(err)
        {
            return next(err);
        }
        else
        {
            res.json(category);
        }
    });
};

module.exports.createCategory = (req,res,next)=>{
    
    categoryModel.create(req.body, (err,category)=>{
        if(err)
        {
            return next(err);
        }
        else
        {
            //console.log(Date.now());
            //console.log(category);
            res.json(category);
        }
    });
};

module.exports.updateCategory = (req, res, next)=>{
    categoryModel.findByIdAndUpdate(req.params.id, req.body, (err,category)=>{
        if(err)
        {
            return next(err);
        }
        else
        {
            res.json(category);
        }
    });
};

module.exports.deleteCategory = (req,res,next) =>{
    categoryModel.findByIdAndRemove(req.params.id, req.body, (err,category)=>{
        if(err)
        {
            return next(err);
        }
        else
        {
            res.json(category);
        }
    })
}