const mongoose = require('mongoose');
const passport = require('passport');
const _ = require('lodash');

const User = mongoose.model('User');

module.exports.register = (req, res, next) => {
    var user = new User();
    //console.log(req.body);
    user.fullName = req.body.fullName;
    user.username = req.body.username;
    user.email = req.body.email;
    user.password = req.body.password;
    user.telephoneNumber = '+84' + req.body.telephoneNumber;
    //console.log(user.telephoneNumber);
    user.save((err, doc) => {
        if (!err)
        {
            console.log(doc);
            res.send(doc);
        }
        else 
        {
            //console.log(err);
            if(err.code == 11000)
            {
                console.log(err.code);
                res.status(422).send(err);
            }
            else
            {
                return next(err);
            }
        }
    });
}

module.exports.login = (req, res, next) => {
    // call for passport authentication
    passport.authenticate('local', (err, user, info) => {       
        // error from passport middleware
        if (err)
        {
            return res.status(400).json(err);
        } 
        // registered user
        else 
        {
            if (user) 
            {
                return res.status(200).json({ "token": user.generateJwt() });
            }
            else
            {
                return res.status(404).json(info);
            }
        }
    })(req, res);
}

module.exports.userProfile = (req, res, next) =>{
    User.findOne({ _id: req._id },
        (err, user) => {
            if (!user)
            {
                return res.status(404).json({ status: false, message: 'User record not found.' });
        
            }
            else
            {
                return res.status(200).json({ status: true, user : _.pick(user,['fullName','username','email','telephoneNumber']) });
            }
         }
    );
}