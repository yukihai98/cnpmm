var postModel = require('../models/post.model');

module.exports.getAllPost = (req,res,next)=>{
    postModel.find((err,posts)=>{
        if(err)
        {
            return next(err);
        }
        else
        {
            res.json(posts)
        }
    });
};

module.exports.getPostById = (req,res,next)=>{
    postModel.findById(req.params.id, (err,post)=>{
        if(err)
        {
            return next(err);
        }
        else
        {
            console.log(post);
            res.json(post);
        }
    });
};

module.exports.createPost = (req,res,next)=>{
    postModel.create(req.body, (err,post)=>{
        if(err)
        {
            return next(err);
        }
        else
        {
            res.json(post);
        }
    });
};

module.exports.updatePost = (req,res,next)=>{
    postModel.findByIdAndUpdate(req.params.id, (err,post)=>{
        if(err)
        {
            return next(err);
        }
        else
        {
            res.json(post);
        }
    });
};


module.exports.deletePost = (req,res,next)=>{
    postModel.findByIdAndRemove(req.params.id, (err,post)=>{
        if(err)
        {
            return next(err);
        }
        else
        {
            res.json(post);
        }
    });
};
