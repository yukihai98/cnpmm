//requirement
require('./config/config');
require('./models/database');
require('./config/passportConfig');

//define 
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const logger = require('morgan');
const cookieParser = require('cookie-parser');

//router
const userRouter = require('./routes/user.router');
const postRouter = require('./routes/post.route');
const categoryRouter = require('./routes/category.route');
const homeRouter = require('./routes/home.route');
const imageRouter = require('./routes/image.route');

var app = express();

//middleware

// Then pass them to cors:
app.use(cors());
app.use(passport.initialize());
app.use(logger('dev'));
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

  
//router
//user
app.use('/api/auth', userRouter);
//post
app.use('/api/post', postRouter);
//category
app.use('/api/category', categoryRouter);
//home 
app.use('/api/public', homeRouter);
//image upload
app.use('/images', imageRouter);

//error handler
app.use((err,req,res,next)=>{
    if(err.name === 'ValidationError')
    {
        var valErrors = [];
        Object.keys(err.errors).forEach(key=>valErrors.push(err.errors[key].message));
        res.status(422).send(valErrors);
    }
    else
    {
        console.log(err);
    }
});

//start server 
console.log(process.env.MONGODB_URI);
app.listen(process.env.PORT, (err)=>{
    if(err)
    {
        console.log('Error when starting server' + err);
    }
    else
    {
        console.log(`Server started at port: ${process.env.PORT}`);
    }
})